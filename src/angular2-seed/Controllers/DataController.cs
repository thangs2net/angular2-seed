﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using aspnetcore_angular2_ngd.Models;
using Microsoft.AspNetCore.Mvc;

namespace aspnetcore_angular2_ngd.Controllers
{
    [Route("api/[Controller]")]
    public class DataController : Controller
    {
        public IEnumerable<Shipment> Get()
        {
            return new List<Shipment>
            {
                new Shipment
                {
                    Id = 1,
                    Origin = "HCMc, Vietnam",
                    Destination = "Phnom Penh",
                    ShippedDate = DateTime.Now
                },
                new Shipment
                {
                    Id = 2,
                    Origin = "Phnom Penh, Cambodia",
                    Destination = "New Zealand",
                    ShippedDate = DateTime.Now
                }
            };
        }
    }
}

﻿using System;

namespace aspnetcore_angular2_ngd.Models
{
    public class Shipment
    {
        public int Id { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public DateTime ShippedDate { get; set; }
    }
}
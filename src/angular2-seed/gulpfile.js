﻿/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var app = "./wwwroot/app";

var gulp = require('gulp'),
    rimraf = require('rimraf');

var ts = require("gulp-typescript");
var tsProject = ts.createProject("./scripts/tsconfig.json", { sortOutput: true });

var path = {
    npm: './node_modules/',
    lib: './wwwroot/lib/'
};

gulp.task('clean:lib', function (cb) {
    return rimraf("./wwwroot/lib", cb);
});

gulp.task('clean:node_modules', function (cb) {
    return rimraf("./node_modules", cb);
});

gulp.task('copy:lib', function () {
    
    return gulp.src('node_modules/**/*').pipe(gulp.dest(path.lib));
});

gulp.task('scripts', function () {
    var tsResult = tsProject.src() // instead of gulp.src(...)
        .pipe(ts(tsProject));

    return tsResult.js.pipe(gulp.dest(app));
});
//import { Component } from '@angular/core';
////import { Http, HTTP_PROVIDERS, Response } from '@angular/http';
////import { Shipment } from '../model';
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
//@Component({
//    selector: 'my-app',
//    template: '<h1>My First Angular 2 App - base on Asp.net Core</h1>'
//    //templateUrl: 'app/partials/app.html',
//    //directives: []
//})
//export class AppComponent {
//    //public shipments: Array<Shipment> = [];
//    //constructor(public http: Http) {
//    //}
//    //getData() {
//    //    return this.http.get('http://localhost:5000/api/data')
//    //        //.forEach(shipment => { this.shipments.push(shipment) });
//    //        //.map((res: Response) => res.json())
//    //        .subscribe(
//    //            data => {
//    //                this.shipments = data;
//    //                console.log(this.shipments);
//    //            },
//    //            err => console.log(err)
//    //        );
//    //}
//}
var core_1 = require('@angular/core');
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: '<h1>Asp.net Core - Angular 2 - Gulp - Seeding</h1>'
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;

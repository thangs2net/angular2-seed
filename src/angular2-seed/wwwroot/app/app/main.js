/// <reference path="../../typings/globals/core-js/index.d.ts" />
"use strict";
//import { bootstrap }    from '@angular/platform-browser-dynamic';
//import { AppComponent } from './app.component';
//bootstrap(AppComponent);
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_module_1 = require('./app.module');
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule);
